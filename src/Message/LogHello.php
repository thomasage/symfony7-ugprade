<?php
declare(strict_types=1);

namespace App\Message;

final readonly class LogHello
{
    public function __construct(public int $length)
    {
    }
}
