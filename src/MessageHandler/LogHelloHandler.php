<?php
declare(strict_types=1);

namespace App\MessageHandler;

use App\Message\LogHello;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class LogHelloHandler
{
    public function __construct(private LoggerInterface $logger)
    {
    }

    public function __invoke(LogHello $message): void
    {
        $this->logger->warning(str_repeat('🎸', $message->length) . ' ' . $message->length);
    }
}
